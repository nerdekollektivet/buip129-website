export interface IAuth {
    readonly challenge: string;
    readonly address: string;
    readonly signature: string;
}

export interface IAuthFirebase {
    readonly id: string;
    readonly provider: string;
}
