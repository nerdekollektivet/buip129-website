import firebase from 'firebase';
import admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/functions';
import { firebaseProjectCfg, getRegion, getProjectId } from './cfg';

export function enableTestMode() {
  // eslint-disable-next-line global-require
  const functionsTest = require('firebase-functions-test');
  const test = functionsTest(firebaseProjectCfg(), `keys/${getProjectId()}.json`);
  return test;
}

export function firebaseCliApp(): firebase.app.App {
  if (firebase.apps.length !== 0) {
    return firebase.apps[0];
  }
  firebase.initializeApp(firebaseProjectCfg());
  return firebaseCliApp();
}

/**
 * You most likely do not want to use this, but firebaseAdminFirestore instead.
 */
export function firebaseCliFirestore(): firebase.firestore.Firestore {
  return firebaseCliApp().firestore();
}

export function firebaseCliAuth(): firebase.auth.Auth {
  return firebaseCliApp().auth();
}

export function firebaseCliFunctions(): firebase.functions.Functions {
  return firebaseCliApp().functions(getRegion());
}

export function firebaseAdminApp(): admin.app.App {
  if (admin.apps.length !== 0) {
    return admin.apps[0] as admin.app.App;
  }
  admin.initializeApp(firebaseProjectCfg());
  return firebaseAdminApp();
}

export function firebaseAdminFirestore(): admin.firestore.Firestore {
  return firebaseAdminApp().firestore();
}

export function firebaseAdminAuth(): admin.auth.Auth {
  return firebaseAdminApp().auth();
}

export function logger(): any {
  return functions.logger;
}

export async function verifyIdToken(idToken: string) {
  return firebaseAdminAuth().verifyIdToken(idToken);
}

export function firebaseFunctions(): any {
  // ensure initialized
  firebaseAdminApp();
  return functions;
}
