import { enableTestMode } from '../db';
import { hasFirebase } from '../testutil';
import { getMockAddress, getMockSignature } from '../mock';
import {
  storeRingsigLink,
  addUserAllowedTags,
} from '../user/user_database';
import {
  createElection,
  getLinkedKeys,
  getElectionDetails,
  listPublicElections,
  listHostedElections,
  listParticipatingElections,
} from './election_endpoints';
import {
  deleteElectionsWithTag,
  setElectionOwner,
} from './election_database';
import {
  CONTRACT_TYPE_RING_SIGNATURE,
  VISIBILITY_PARTICIPANTS,
  VISIBILITY_PUBLIC,
  CONTRACT_TYPE_MULTI_OPTION_VOTE,
} from './model/election';

import { AuthenticationError, MissingParamError, InvalidParamError } from '../error';
import { DUMMYUSER_ADDRESS, dummyuserCleanup } from '../rpcauth';

enableTestMode();
const skipIf = require('skip-if');

function mocReqTemplate(): any {
  return {
    body: {
      options: ['hello', 'world', 'foobar'],
      participants: [getMockAddress()],
      description: 'Test election',
      contract_type: CONTRACT_TYPE_MULTI_OPTION_VOTE,
      end_height: 100_000,
      visibility: VISIBILITY_PARTICIPANTS,
    },
  };
}

const LIST_ELECTION_TEST_TAG = 'test-listelections';

describe('createElection', () => {
  afterEach(async () => Promise.all([
    dummyuserCleanup,
    deleteElectionsWithTag(LIST_ELECTION_TEST_TAG),
  ]));

  skipIf(!hasFirebase(), 'missing_param', async () => {
    const mocRes = {};
    const mocReq = mocReqTemplate();
    delete mocReq.body.options;
    const dummy = true;

    const t = async () => {
      await createElection(mocReq, mocRes, dummy);
    };

    await expect(t).rejects.toThrow(MissingParamError);
  });

  skipIf(!hasFirebase(), 'createRingSignatureVote', async () => {
    const mocRes = {};
    const mocReq = mocReqTemplate();
    mocReq.body.contract_type = CONTRACT_TYPE_RING_SIGNATURE;
    mocReq.body.begin_height = 100;
    const dummy = true;

    // Ensure moc participant has linked a dummy ring signauter pubkey
    const dummyPubkey = Buffer.alloc(32, 'X').toString('hex');
    const message = `link:${dummyPubkey}`;
    const signature = getMockSignature(message);
    await storeRingsigLink(getMockAddress(), dummyPubkey, message, signature);

    // Create election
    const result = JSON.parse(await createElection(mocReq, mocRes, dummy));
    expect(result.status).toBe('Election created');
    expect(result.election_id).toMatch(/^[a-zA-Z0-9]+$/);
  });

  /**
     * Try adding a user that has no ringsig link to an election
     */
  skipIf(!hasFirebase(), 'createRingSignatureVote_no_link', async () => {
    const mocRes = {};
    const mocReq = mocReqTemplate();
    mocReq.body.contract_type = CONTRACT_TYPE_RING_SIGNATURE;
    mocReq.body.begin_height = 100;

    // This participant does not have a ring signature pubkey link
    mocReq.body.participants.push('bitcoincash:qzxqjtkze0vy96y5xtru2w65mvaftryr55yzmhy8sl');

    const dummy = true;

    // Create election
    const t = async () => {
      await createElection(mocReq, mocRes, dummy);
    };
    await expect(t).rejects.toThrow(InvalidParamError);
    await expect(t).rejects.toThrow(/has not enabled anonymous voting/);
  });

  skipIf(!hasFirebase(), 'getLinkedKeys', async () => {
    const mocRes = {};
    const mocReq = {
      query: { election_id: 'nonexisting' },
    };
    const dummy = true;

    const t = async () => {
      await getLinkedKeys(mocReq, mocRes, dummy);
    };
    await expect(t).rejects.toThrow(InvalidParamError);
  });

  skipIf(!hasFirebase(), 'getElectionDetails', async () => {
    let res: any = null;

    const resToQuery = (resultJSON: string) => {
      const result = JSON.parse(resultJSON);
      return {
        query: {
          election_id: result.election_id,
        },
        headers: {
        },
      };
    };
    // Create an election with public visibility. We should be able to request
    // its details without authenticating.
    const mocReq = mocReqTemplate();
    const mocRes = {};
    mocReq.body.begin_height = 100;
    mocReq.body.visibility = VISIBILITY_PUBLIC;
    res = await createElection(mocReq, mocRes, true /* dummyuser */);
    let election = await getElectionDetails(
      resToQuery(res), mocRes,
      false, /* dummyuser: false to no authenticate as dummy user */
    );
    expect(JSON.parse(election).participantAddresses[0]).toBe(getMockAddress());

    // Now create an election with participant visibility. We should not be able to
    // request its details.
    mocReq.body.visibility = VISIBILITY_PARTICIPANTS;
    res = await createElection(mocReq, mocRes, true /* dummyuser */);
    const t = async () => {
      await getElectionDetails(resToQuery(res), mocRes, false /* dummyuser */);
    };
    await expect(t).rejects.toThrow(AuthenticationError);
    await expect(t).rejects.toThrow(/not public and callee is not authenticated/);

    // "dummyuser" is the creator of the above election, so this should not throw.
    election = await getElectionDetails(resToQuery(res), mocRes, true /* dummyuser */);
    expect(JSON.parse(election).participantAddresses[0]).toBe(getMockAddress());
  });

  skipIf(!hasFirebase(), 'createElectionWithTags', async () => {
    const mocRes = {};
    const mocReq = mocReqTemplate();
    mocReq.body.begin_height = 100;
    const dummy = true;

    // Throws when using a tag user does not have access to.
    mocReq.body.tags = ['test-not-allowed-tag'];

    const t = async () => {
      await createElection(mocReq, mocRes, dummy);
    };

    await expect(t).rejects.toThrow(InvalidParamError);
    await expect(t).rejects.toThrow(/User is not allowed to use tag/);

    // Allow creation with allowed tag
    await addUserAllowedTags(DUMMYUSER_ADDRESS, ['allowed-tag']);
    mocReq.body.tags = ['allowed-tag'];
    const result = await createElection(mocReq, mocRes, dummy);
    expect(JSON.parse(result).status).toBe('Election created');
  });

  skipIf(!hasFirebase(), 'listPublicElections', async () => {
    const req = {
      query: {
        filter_tags: LIST_ELECTION_TEST_TAG,
      },
    };
    const emptyResult = JSON.parse(await listPublicElections(req, { }));
    expect(emptyResult.cursor).toBe(null);
    expect(emptyResult.elections).toStrictEqual([]);

    // Create two elections, one with public visibility, one without. Only
    // the public one should be returned.
    await addUserAllowedTags(DUMMYUSER_ADDRESS, [LIST_ELECTION_TEST_TAG]);

    const mocReq = mocReqTemplate();
    mocReq.body.begin_height = 100;
    mocReq.body.tags = [LIST_ELECTION_TEST_TAG];
    mocReq.body.visibility = VISIBILITY_PUBLIC;
    const publicElectionID = JSON.parse(await createElection(mocReq, { }, true)).election_id;

    mocReq.body.visibility = VISIBILITY_PARTICIPANTS;
    await createElection(mocReq, { }, true);

    const list = JSON.parse(await listPublicElections(req, { }));
    expect(list.elections.length).toBe(1);
    expect(list.elections[0].id).toBe(publicElectionID);
  });

  skipIf(!hasFirebase(), 'listHostedElections', async () => {
    const req = {
      query: {
        filter_tags: LIST_ELECTION_TEST_TAG,
      },
    };
    const skipAuth = true;
    const emptyResult = JSON.parse(await listHostedElections(req, { }, skipAuth));
    expect(emptyResult.cursor).toBe(null);
    expect(emptyResult.elections).toStrictEqual([]);

    // Create two elections, one with public visibility, one without. Both
    // should be returned.
    await addUserAllowedTags(DUMMYUSER_ADDRESS, [LIST_ELECTION_TEST_TAG]);

    const mocReq = mocReqTemplate();
    mocReq.body.tags = [LIST_ELECTION_TEST_TAG];
    mocReq.body.visibility = VISIBILITY_PUBLIC;
    mocReq.body.begin_height = 100;
    const electionID1 = JSON.parse(await createElection(mocReq, { }, true)).election_id;

    mocReq.body.visibility = VISIBILITY_PARTICIPANTS;
    const electionID2 = JSON.parse(await createElection(mocReq, { }, true)).election_id;

    const list = JSON.parse(await listHostedElections(req, { }, skipAuth));
    expect(list.elections.length).toBe(2);

    // Order should be creation time, newest first.
    console.log(electionID1, electionID2);
    expect(list.elections[0].id).toBe(electionID2);
    expect(list.elections[1].id).toBe(electionID1);
  });

  jest.setTimeout(10000);
  skipIf(!hasFirebase(), 'listParticipatingElections', async () => {
    const req = {
      query: {
      },
    };
    const skipAuth = true;
    const emptyResult = JSON.parse(await listParticipatingElections(req, { }, skipAuth));
    expect(emptyResult.cursor).toBe(null);
    expect(emptyResult.elections).toStrictEqual([]);

    // Create two elections, one with public visibility, one without. Add ourselves
    // as participant, Both should be returned.
    await addUserAllowedTags(DUMMYUSER_ADDRESS, [LIST_ELECTION_TEST_TAG]);

    const mocReq = mocReqTemplate();
    mocReq.body.tags = [LIST_ELECTION_TEST_TAG];
    mocReq.body.visibility = VISIBILITY_PUBLIC;
    mocReq.body.participants.push(DUMMYUSER_ADDRESS);
    mocReq.body.begin_height = 100;

    const electionID1 = JSON.parse(await createElection(mocReq, { }, true)).election_id;

    mocReq.body.visibility = VISIBILITY_PARTICIPANTS;
    const electionID2 = JSON.parse(await createElection(mocReq, { }, true)).election_id;

    // Make someone else the owner of the elections, we want to only be a
    // participant.
    await Promise.all([electionID1, electionID2].map(
      (id) => setElectionOwner(id, getMockAddress()),
    ));
    const list = JSON.parse(await listParticipatingElections(req, { }, skipAuth));
    console.log(list);
    expect(list.elections.length).toBe(2);

    // Order should be creation time, newest first.
    console.log(electionID1, electionID2);
    expect(list.elections[0].id).toBe(electionID2);
    expect(list.elections[1].id).toBe(electionID1);
  });

  skipIf(!hasFirebase(), 'createMultiOptionVote', async () => {
    const mocRes = {};
    const mocReq = mocReqTemplate();
    mocReq.body.contract_type = CONTRACT_TYPE_MULTI_OPTION_VOTE;
    mocReq.body.begin_height = 100;
    const dummy = true;

    // Create election

    const responseJSON = await createElection(mocReq, mocRes, dummy);
    console.log('election returned: ', responseJSON);

    const result = JSON.parse(responseJSON);
    expect(result.status).toBe('Election created');
    expect(result.election_id).toMatch(/^[a-zA-Z0-9]+$/);
  });
});
