import {
  BLANK_VOTE,
  Blockchain,
  MultiOptionVote,
  RingSignatureVote,
  TwoOptionVote,
  fetchMultiOptionVotes,
  fetchRingSignatureVotes,
  hash160Salted,
  tallyTwoOptionVotes,
} from '@bitcoinunlimited/votepeerjs';
import { CashAddressType, decodeCashAddress } from '@bitauth/libauth';
import { Election } from './model/election';
import { getRingSignaturePubkey } from '../user/user_database';
import ServerError from '../error/servererror';

function addressToPKH(address: string): Buffer {
  const decoded: any = decodeCashAddress(address);
  if (decoded.type !== CashAddressType.P2PKH) {
    throw Error('Invalid election address');
  }
  return decoded.hash;
}

export async function getTwoOptionVoteTally(
  electrum: Blockchain,
  election: Election,
): Promise<any> {
  const votersPKH = election.participantAddresses.map((address: string) => addressToPKH(address));

  const e = {
    network: 'mainnet',
    salt: Buffer.from(election.salt),
    description: election.description,
    optionA: election.options[0],
    optionB: election.options[1],
    votersPKH,
    endHeight: election.endHeight,
  } as TwoOptionVote;

  const optionAHash = Buffer.from(await hash160Salted(e.salt, Buffer.from(e.optionA)));
  const optionBHash = Buffer.from(await hash160Salted(e.salt, Buffer.from(e.optionB)));
  const blank = Buffer.from(BLANK_VOTE);

  function hashToOption(hash: Uint8Array) {
    if (optionAHash.equals(hash)) {
      return e.optionA;
    }
    if (optionBHash.equals(hash)) {
      return e.optionB;
    }
    if (blank.equals(hash)) {
      return 'Blank';
    }
    throw new ServerError('Invalid vote in an accepted ballot');
  }

  const ballots = await tallyTwoOptionVotes(
    electrum,
    e,
    true, /* include unconfirmed */
    false, /* don't include rejected */
  );
  return Promise.all(ballots.accepted.map(async (b) => ({
    txid: b.transaction.getTxID(),
    vote_option: hashToOption(b.vote),
    vote_option_hash: Buffer.from(b.vote).toString('hex'),
    participant: b.participant,
    confirmed_height: b.transaction.height,
  })));
}

export async function getRingSignatureVoteTally(
  electrum: Blockchain,
  election: Election,
): Promise<any> {
  /* eslint-disable-next-line max-len */
  const pubkeysHex = await Promise.all(election.participantAddresses.map((user: string) => getRingSignaturePubkey(user)));

  const pubkeys = pubkeysHex.map((p) => Buffer.from(p, 'hex'));

  const ringSignatureElection = new RingSignatureVote(
    Buffer.from(election.salt), election.description, election.beginHeight,
    election.endHeight, election.options, pubkeys,
  );

  const ballots = await fetchRingSignatureVotes(
    electrum,
    ringSignatureElection,
    false, /* don't include rejected */
  );

  return Promise.all(ballots.accepted.map(async (b) => ({
    txid: b.transaction.getTxID(),
    vote_option: await ringSignatureElection.getOptionFromHash(b.vote),
    vote_option_hash: Buffer.from(b.vote).toString('hex'),
    participant: b.participant,
    confirmed_height: b.transaction.height,
  })));
}

export async function getMultiOptionVoteTally(
  electrum: Blockchain,
  election: Election,
): Promise<any> {
  const votersPKH = election.participantAddresses
    .map((address: string) => addressToPKH(address));

  const movElection = new MultiOptionVote(
    Buffer.from(election.salt),
    election.description,
    election.beginHeight,
    election.endHeight,
    election.options,
    votersPKH,
  );

  const ballots = await fetchMultiOptionVotes(electrum, movElection);

  return Promise.all(ballots.accepted.map(async (b) => ({
    txid: b.transaction.getTxID(),
    vote_option: await movElection.getOptionFromHash(b.vote),
    vote_option_hash: Buffer.from(b.vote).toString('hex'),
    participant: b.participant,
    confirmed_height: b.transaction.height,
  })));
}
