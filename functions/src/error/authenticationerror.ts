/**
 * Error thrown when a un-authenticated user attempts to access API
 * functionality that requires authentication.
 */
export default class AuthenticationError extends Error { }
