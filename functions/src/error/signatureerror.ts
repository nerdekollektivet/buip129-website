/**
 * Errors related to verifying signature.
 */
export default class SignatureError extends Error { }
