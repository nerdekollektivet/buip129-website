import { logger, verifyIdToken } from './db';
import {
  AuthenticationError,
} from './error';
import { deleteUser } from './user/user_database';

/**
 * Address used in unit tests when skipping/faking authentication.
 */
export const DUMMYUSER_ADDRESS = 'bitcoincash:qqqqqqqqqqqqqqqqqqqqqqqqqqqqqu08dsyxz98whc';

// eslint-disable-next-line import/prefer-default-export
export async function getAuthenticatedUser(req: any, dummyuser: boolean) {
  if (dummyuser) {
    return { user_id: DUMMYUSER_ADDRESS };
  }

  logger().log('Check if request is authorized with Firebase ID token');

  if ((!req.headers.authorization
      || !req.headers.authorization.startsWith('Bearer '))
  // eslint-disable-next-line no-underscore-dangle
      && !(req.cookies && req.cookies.__session)) {
    logger().log('Request headers: ', JSON.stringify(req.headers));
    throw new AuthenticationError(
      'No Firebase ID token was passed as a Bearer token in the Authorization header.'
      + 'Make sure you authorize your request by providing the following HTTP header:'
      + 'Authorization: Bearer <Firebase ID Token>'
      + 'or by passing a "__session" cookie.',
    );
  }

  let idToken;
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
    logger().log('Found "Authorization" header');
    // Read the ID Token from the Authorization header.
    // eslint-disable-next-line prefer-destructuring
    idToken = req.headers.authorization.split('Bearer ')[1];
  } else if (req.cookies) {
    logger().log('Found "__session" cookie');
    // Read the ID Token from cookie.
    // eslint-disable-next-line no-underscore-dangle
    idToken = req.cookies.__session;
  } else {
    throw new AuthenticationError('No cookie');
  }

  let decodedIdToken = null;
  try {
    decodedIdToken = await verifyIdToken(idToken);
    logger().log('ID Token correctly decoded', decodedIdToken);
  } catch (error) {
    throw new AuthenticationError(`Error while verifying Firebase ID token: ${error}`);
  }
  if (decodedIdToken.isAnonymous) {
    throw new AuthenticationError(`User is only authenticated as anonymous user. ${decodedIdToken}`);
  }
  return decodedIdToken;
}

export async function dummyuserCleanup(): Promise<any> {
  return deleteUser(DUMMYUSER_ADDRESS);
}
