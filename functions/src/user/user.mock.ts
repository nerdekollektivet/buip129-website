import { IUser } from './model/user';

// eslint-disable-next-line
export function mockUserData(): IUser {
  const mockUser: IUser = {
    id: 'id',
    displayName: 'string',
    fcmToken: 'string',
    imageUrl: 'string',
  };

  return mockUser;
}
