import { enableTestMode } from '../db';
import { hasFirebase } from '../testutil';
import { getMockAddress, getMockSignature } from '../mock';
import { linkPublicKey, LINK_PURPOSE, listUserAllowedTags } from './user_endpoint';
import { deleteRingSigLink, addUserAllowedTags } from './user_database';
import {
  InvalidParamError, MissingParamError, SignatureError, UserError, DoesNotExistError,
} from '../error';
import { DUMMYUSER_ADDRESS, dummyuserCleanup } from '../rpcauth';

enableTestMode();
const skipIf = require('skip-if');

let mocResResult: any = {

};
const mocRes = {
  set: (k: string, v: string) => { /* ignore */ },
  setHeader: (k: string, v: string) => { /* ignore */ },
  status: (code: number) => {
    mocResResult.status = code;
    return {
      send: (send: string) => {
        mocResResult.send = send;
      },
    };
  },
};

function mocReqTemplate(): any {
  const pubkey = Buffer.alloc(32, 'A').toString('hex');
  return {
    body: {
      userid: getMockAddress(),
      pubkey,
      purpose: LINK_PURPOSE.RingSignature,
      signature: getMockSignature(`link:${pubkey}`),
    },
  };
}

describe('linkPublicKey', () => {
  jest.setTimeout(30000); // for invalid_signature test case
  afterEach(async () => {
    mocResResult = { };

    // Delete link, if any.
    try {
      await deleteRingSigLink(mocReqTemplate().body.userid);
    } catch (e) {
      if (e instanceof DoesNotExistError) {
        // Link not found. Ignore.
        return;
      }
      throw e;
    }
  });

  skipIf(!hasFirebase(), 'invalid_purpose', async () => {
    const mocReq = mocReqTemplate();
    mocReq.body.purpose = 'invalid';

    const t = async () => {
      await linkPublicKey(mocReq, mocRes);
    };
    await expect(t).rejects.toThrow(InvalidParamError);
    await expect(t).rejects.toThrow(/Parameter 'purpose' has invalid value/);
  });

  skipIf(!hasFirebase(), 'invalid pubkey', async () => {
    const mocReq = mocReqTemplate();
    mocReq.body.pubkey = 'not a hex string';

    const t = async () => {
      await linkPublicKey(mocReq, mocRes);
    };
    await expect(t).rejects.toThrow(InvalidParamError);
    await expect(t).rejects.toThrow(/Must be.*hexadecimal/);

    // valid hex, but wrong length
    mocReq.body.pubkey = 'beef';
    const t2 = async () => {
      await linkPublicKey(mocReq, mocRes);
    };
    await expect(t2).rejects.toThrow(InvalidParamError);
    await expect(t2).rejects.toThrow(/Invalid length/);
  });

  skipIf(!hasFirebase(), 'invalid_signature', async () => {
    const mocReq = mocReqTemplate();

    // case: no signature
    delete mocReq.body.signature;
    const t = async () => {
      await linkPublicKey(mocReq, mocRes);
    };
    await expect(t).rejects.toThrow(MissingParamError);
    await expect(t).rejects.toThrow(/signature/);

    // case: garbage
    mocReq.body.signature = 'badbeef';
    const t2 = async () => {
      await linkPublicKey(mocReq, mocRes);
    };
    await expect(t2).rejects.toThrow(SignatureError);
    await expect(t2).rejects.toThrow(/Signature verification error/);

    // case: wrong thing signed
    mocReq.body.signature = getMockSignature('wrong message');
    const t3 = async () => {
      await linkPublicKey(mocReq, mocRes);
    };
    await expect(t3).rejects.toThrow(SignatureError);
    await expect(t3).rejects.toThrow(/Signature verification failed/);
  });

  skipIf(!hasFirebase(), 'valid_ringsig_link', async () => {
    const mocReq = mocReqTemplate();
    const result = await linkPublicKey(mocReq, mocRes);
    expect(JSON.parse(result).status).toMatch(/^Linked/);
  });

  skipIf(!hasFirebase(), 'ok_to_link_same_pubkey_twice', async () => {
    const mocReq = mocReqTemplate();
    await linkPublicKey(mocReq, mocRes);
    const result = await linkPublicKey(mocReq, mocRes);
    expect(JSON.parse(result).status).toMatch(/^Linked/);
  });

  skipIf(!hasFirebase(), 'linking_different_pubkey_fails', async () => {
    const mocReq = mocReqTemplate();
    await linkPublicKey(mocReq, mocRes);

    const otherPubkey = Buffer.alloc(32, 'B').toString('hex');
    mocReq.body.pubkey = otherPubkey;
    mocReq.body.signature = getMockSignature(`link:${otherPubkey}`);

    const t = async () => {
      await linkPublicKey(mocReq, mocRes);
    };
    await expect(t).rejects.toThrow(UserError);
    await expect(t).rejects.toThrow(/User already has a different public key linked/);
  });
});

describe('User tags tests', () => {
  beforeEach(dummyuserCleanup);
  afterEach(dummyuserCleanup);

  skipIf(!hasFirebase(), 'list_user_allowed_tags', async () => {
    const user = DUMMYUSER_ADDRESS;
    const mocReq = {
      query: {
        user_id: user,
      },
    };
    const skipAuth = true;

    const tagsJSON = await listUserAllowedTags(mocReq, {}, skipAuth);
    expect(JSON.parse(tagsJSON).tags).toStrictEqual([]);

    await addUserAllowedTags(user, ['hello', 'world']);
    const tagsJSON2 = await listUserAllowedTags(mocReq, {}, skipAuth);
    expect(JSON.parse(tagsJSON2).tags.sort()).toStrictEqual(['hello', 'world'].sort());
  });
});
