import firebase from 'firebase/app';
import bchaddr from 'bchaddrjs';
import { firestore } from '../db';

export interface IGroup {
    readonly groupId: string;
    readonly owner: string;
    readonly participants: string[];
    readonly title: string;
}

function toIGroup(doc: any): IGroup {
  const data = doc?.data();
  if (doc?.data === undefined) {
    throw Error('Group data missing');
  }
  return {
    groupId: doc.id,
    participants: data.participants,
    owner: data.owner,
    title: data.title,
  } as IGroup;
}

const GROUP_COLLECTION = 'group';

/**
 * Database access to group functionality
 */
export class GroupDB {
  async create(
    owner: String,
    title: String,
    participants: String[],
  ): Promise<string> {
    const group = await firestore().collection(GROUP_COLLECTION)
      .add({
        owner,
        participants,
        title,
      })
      .catch((error) => {
        throw Error(error.message);
      });

    return group.id;
  }

  async get(groupId: string): Promise<IGroup> {
    const snap = await firestore().collection(GROUP_COLLECTION)
      .doc(groupId)
      .get()
      .catch((error) => {
        throw Error(error);
      });

    if (!snap) {
      throw Error('Group not found');
    }

    return toIGroup(snap);
  }

  async delete(groupId: string): Promise<void> {
    return firestore().collection(GROUP_COLLECTION)
      .doc(groupId)
      .delete()
      .catch((error) => {
        throw Error(error);
      });
  }

  /**
   * Add a participant to an existing group
   */
  async addParticipant(groupId: string, user: string): Promise<void> {
    console.log(`Add participant ${user} to ${groupId}`);

    if (!bchaddr.isValidAddress(user)) {
      throw Error('Not a valid Bitcoin Cash address');
    }

    const groupRef = firestore().collection(GROUP_COLLECTION).doc(groupId);
    return groupRef.update({
      participants: firebase.firestore.FieldValue.arrayUnion(user),
    });
  }

  /**
   * Remove a participant from a group.
   */
  async removeParticipant(groupId: string, user: string): Promise<void> {
    const groupRef = firestore().collection(GROUP_COLLECTION).doc(groupId);
    return groupRef.update({
      participants: firebase.firestore.FieldValue.arrayRemove(user),
    });
  }

  /**
   * Get groups that the user is participant of.
   */
  async getParticipantOf(user: string): Promise<Array<IGroup>> {
    const snap = await firestore().collection(GROUP_COLLECTION)
      .where('participants', 'array-contains', user)
      .get()
      .catch((error: any) => {
        throw Error(error.message);
      });

    if (!snap) {
      throw Error('Failed to retrieve participant groups');
    }

    const groups: Array<IGroup> = [];

    snap.forEach((doc: any) => {
      groups.push(toIGroup(doc));
    });

    return groups;
  }

  /**
   * Get groups that the user is owner of.
   */
  async getOwnerOf(owner: string): Promise<Array<IGroup>> {
    const snap = await firestore().collection(GROUP_COLLECTION)
      .where('owner', '==', owner)
      .get()
      .catch((error: any) => {
        throw Error(error.message);
      });

    if (!snap) {
      throw Error('Failed to retrieve owned groups');
    }

    const groups: Array<IGroup> = [];
    snap.forEach((doc: any) => {
      groups.push(toIGroup(doc));
    });

    return groups;
  }
}
