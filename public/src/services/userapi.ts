import authService from './auth';
import { function_url } from '../db';

export default class UserAPI {
  /**
   * Get tags that user is allowed to assign to elections.
   */
  static async listAllowedTags(): Promise<any> {
    const url = function_url('list_user_allowed_tags');
    const url_options = {
      headers: await authService.getRequestHeaders(),
    };
    const res: Response = await fetch(url, url_options);
    if (!res.ok) {
      throw Error(`Failed to get tags: ${await res.text()}`);
    }
    return res.json();
  }
}
