import {
  VuexModule, Module, Mutation, Action,
} from 'vuex-module-decorators';

// NOTE: Vuex Typescript reference implementation
// Ref: https://blog.logrocket.com/how-to-write-a-vue-js-app-completely-in-typescript/
@Module({ namespaced: true })
class Alert extends VuexModule {
    public isDisplaying: boolean = false

    public content: string = '';

    @Mutation
    public display(content: string): void {
      this.content = content;
      this.isDisplaying = true;

      // scroll to top so that the user sees error
      window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth',
      });
    }

    @Mutation
    public hide(): void {
      this.isDisplaying = false;
      this.content = '';
    }

    @Action
    public displayAlert(content: String): void {
      this.context.commit('display', content);
    }

    @Action
    public hideAlert(): void {
      this.context.commit('hide');
    }
}

export default Alert;
